<?php
session_start();

/*if (!isset($_SESSION))
{
	echo "Vous devez être connecté pour accéder à cette page ! ";
}*/
if (isset($_SESSION['Admin']) || isset($_SESSION['Salarie']))
{
require_once("util/fonction.inc.php");
require_once("util/class.pdoPPE2.inc.php");
include("vues/v_entete.php");
if (isset($_SESSION['Admin']))
{
	include("vues/v_bandeauAdmin.php");
}
else if (isset($_SESSION['Salarie']))
{
	include("vues/v_bandeauSalarie.php");
}
if(!isset($_REQUEST['uc']))
{
	$uc = 'accueil';
}
else
{
	$uc = $_REQUEST['uc'];
}
 $pdo = PdoPPE2::getPdoPPE2();	
switch($uc)
{
	case 'accueil':
            {			
                include("vues/v_accueil.php");
                break;
            }
        case 'espaceClient':
            {
                include("controleurs/c_gestionClient.php");
                break;
            }
        case 'espaceAdmin':
            {
                include("controleurs/c_gestionAdmin.php");
                break;
            }

}
include("vues/v_pied.php") ;
}
else
{
	echo "Vous devez être connecté pour accéder à cette page ! ";
}
?>
