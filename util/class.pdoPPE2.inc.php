<?php

class PdoPPE2
	{
		private static $serveur='mysql:host=127.0.0.1';
      	private static $bdd='dbname=PPE2';   		 
		private static $user='root' ;    		
      	private static $mdp='' ;
		private static $monPdo;
		private static $monPdoPPE2 = null;

		private function __construct()
		{
    		PdoPPE2::$monPdo = new PDO(PdoPPE2::$serveur.';'.PdoPPE2::$bdd, PdoPPE2::$user, PdoPPE2::$mdp); 
			PdoPPE2::$monPdo->query("SET CHARACTER SET utf8");
		}
		
		public function _destruct()
		{
			PdoPPE2::$monPdo = null;
		}

          public function chargerLesUsers($nom,$mdp)
		{
			$req = "SELECT * FROM salarie WHERE mailSalarie='".$nom."' AND mdpSalarie='".$mdp."';";
			$res = PdoPPE2::$monPdo->query($req);
			$lesLignes = $res->fetch();
			if ($lesLignes['mailSalarie'] == $nom && $lesLignes['mdpSalarie'] == $mdp)
			{
			return true;
			}
			return false;
		}
		
		 public function chargerLesAdmins($nom,$mdp)
		{
			$req = "SELECT * FROM administrateur WHERE login='".$nom."' AND mdp='".$mdp."';";
			$res = PdoPPE2::$monPdo->query($req);
			$lesLignes = $res->fetch();
			if ($lesLignes['login'] == $nom && $lesLignes['mdp'] == $mdp)
			{
			return true;
			}
			return false;
		}
		
		
		public  static function getPdoPPE2()
		{
		if(PdoPPE2::$monPdoPPE2 == null)
		{
			PdoPPE2::$monPdoPPE2= new PdoPPE2();
		}
		return PdoPPE2::$monPdoPPE2;  
		}
		
		 public function chargerLesEvenements()
		{
			$req = "SELECT * FROM evenement;";
			$res = PdoPPE2::$monPdo->query($req);
			$lesLignes = $res->fetchAll();
			return $lesLignes; 			
		}
	
		public function modifierEvenement($idEvenement, $date, $heure)
		{
			$req = "UPDATE evenement set dateJourEvenement='".$date."', heureEvenement='".$heure."' where idEvenement= ".$idEvenement." ;";
			$res = PdoPPE2::$monPdo->query($req);					
		}
		
		public function supprimerEvenement($idEvenement)
		{
			$req = "Delete from evenement where idEvenement='".$idEvenement."'";
			$res = PdoPPE2::$monPdo->query($req);					
		}
		
		public function ajouterEvenement($date, $heure, $idEvenement, $libelle, $idSalarie)
		{
			$req = "INSERT INTO evenement VALUES('','".$date."','".$heure."','".$libelle."',".$idEvenement.",".$idSalarie.")";
			$res = PdoPPE2::$monPdo->query($req);
				
			
		}
		
		public function getLaListeDesSalaries()
		{
			$req = "SELECT idSalarie, nomSalarie FROM salarie";
			$res = PdoPPE2::$monPdo->query($req);	
			$lesLignes = $res->fetchAll();
			return $lesLignes; 		
		}
		
		public function getIdSalarie($login)
		{
			$req = "SELECT * FROM salarie WHERE mailSalarie='".$login."' ";
			$res = PdoPPE2::$monPdo->query($req);	
			$lesLignes = $res->fetch();
			
			if ($lesLignes['mailSalarie'] == $login)
			{
			 return $lesLignes['idSalarie'];
			}
			 echo "erreur";
		}
		
		public function EvenementSalarie($idSalarie)
		{
			$req = "SELECT * FROM evenement
					INNER JOIN convoquer
					ON evenement.idEvenement = convoquer.idEvenement
					AND convoquer.idSalarie=".$idSalarie." ";
			
			$res = PdoPPE2::$monPdo->query($req);	
			
			$lesLignes = $res->fetchAll();
			return $lesLignes; 		
		}
		
	
	
	
	
	
	
	
               

}
?>