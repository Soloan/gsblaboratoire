<?php
session_start();

require_once("util/fonction.inc.php");
require_once("util/class.pdoPPE2.inc.php");
include("vues/v_entete.php");
if(!isset($_REQUEST['uc']))
{
	$uc = 'accueil';
}
else
{
	$uc = $_REQUEST['uc'];
}
$pdo = PdoPPE2::getPdoPPE2();	
switch($uc)
{
	case 'accueil':
            {			
                include("vues/v_accueil.php");
                break;
            }
    case 'testerConnexion':
			{
                if(isset($_POST['Valider']))
				{
				$login = $_POST['login'];
				$mdp = $_POST['mdp'];
				if(empty($login) or empty($mdp))
				{
					echo "Vous devez remplir tout les champs";
					include("vues/v_accueil.php");
					
				}
				else
				{
				$lesAdmin = $pdo->chargerLesAdmins($login,$mdp);				
					if ($lesAdmin)
						{
							$_SESSION['Admin'] = $login;
							echo "<script>document.location.replace('index2.php?uc=espaceAdmin&action=accueilAdmin');</script>";
						}
					else
						{
							$lesUsers = $pdo->chargerLesUsers($login,$mdp);
							if ($lesUsers)
								{
									$_SESSION['Salarie'] = $login;
									echo "<script>document.location.replace('index2.php?uc=espaceClient&action=afficherListeEvenement');</script>";
								}
							else
								{
									echo "Erreur de login et/ou mot de passe ! ";
								}
						}
							
					
						
				
				}
				}
                break;
            }
        case 'EspaceClient':
            {
                include("controleurs/c_gestionClient.php");
                break;
            }
        case 'espaceAdmin':
            {
                include("controleurs/c_gestionAdmin.php");
                break;
            }

}
?>

