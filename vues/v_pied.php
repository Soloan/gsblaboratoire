

    
    <!-- /.container -->
	
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p>Copyright &copy; KASTNER Fabien & HAININ Amandine</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="vues/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vues/js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>