

    <div class="brand">Laboratoire GSB</div>
    
    <!-- Navigation -->
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- navbar-brand is hidden on larger screens, but visible when the menu is collapsed -->
                <a class="navbar-brand" href="index.php">Laboratoire GSB</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="index2.php?uc=espaceAdmin&action=accueilAdmin">Liste Evènements </a>
                    </li>
                    <li>
                        <a href="index2.php?uc=espaceAdmin&action=ajoutEvenement">Ajout d'un évènement</a>
                    </li>
                    <li>
                        <a href="index2.php?uc=espaceAdmin">Parcours</a>
                    </li>
                    <li>
                        <a href="index2.php?uc=espaceAdmin">Classement</a>
                    </li>
                    <li>
                        <a href="index2.php?uc=espaceAdmin&action=deconnexion"> Se Déconnecter</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>